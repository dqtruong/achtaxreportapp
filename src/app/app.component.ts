import { Component } from '@angular/core';
import { faCoffee, faUserCircle, faTachometerAlt, faMoneyBillAlt, faCalendarDay } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'AchTaxReportApp';
  faCoffee = faCoffee;
  faUserCircle = faUserCircle;
  faTachometerAlt = faTachometerAlt;
  faMoneyBillAlt = faMoneyBillAlt;
  faCalendarDay = faCalendarDay;
}
